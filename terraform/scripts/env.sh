export USER_NAME="ubuntu"
export PATH_TO_SSH_KEY="/home/user/.ssh/terraform-ssh.pem"
export BASTION_PUBLIC_IP=$(terraform output | grep bastion | cut -d ' ' -f3 | cut -d '"' -f2)